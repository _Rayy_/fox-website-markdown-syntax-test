# FOX AIRSOFT MARKDOWN SYNTAX DEMONSTRATION
```
Typically for all in stock items, any order submitted before 2pm MST generally will be shipped within same day excluding weekends. Orders placed over the weekend are shipped the following Monday. NOTE: Holiday season processing time for orders may take an additional 1-2 business days. 

Due to e-mail volume, e-mails are replied to within 1-2 business days. 

```


## FOX AIRSOFT IS THE BEST ONLINE AIRSOFT SHOP - RETAIL SHOP SERVING DENVER, COLORADO

See our New [30/30 Return & Warranty Policy](img/01.pdf)

<details>
    <summary>We are committed to separating ourselves from the rest of the pack.</summary>
Fox Airsoft is an online and brick and mortar retail store that is the unmatched provider in quality airsoft solutions. Our airsoft store is open Monday through Saturday for your airsoft needs. To see our store hours click here.

We are committed to separating ourselves from the rest of the pack. Our employees play airsoft every weekend to ensure we have the most up to date information on high quality airsoft guns, tactical gear, internal parts, and more. Since we field test everything we sell, you can shop confidently knowing that your airsoft gun purchase at the Fox Airsoft store will be a smart purchase. Our friendly knowledgeable reps are here to help you and answer any questions you may have to make you the best on the field.

Fox Airsoft carries only the best products for our customers from strong brands like Umarex, Elite Force, ICS, LCT, Condor, ASG, NC Star, Echo1 USA, Classic Army, and many more.
</details>

## Our Expertise Sets Us Apart
<details>
    <summary>Did you know that Fox Airsoft is Colorado's only dedicated full time airsoft online and walk in retailer? </summary>
 
Whether you want to take a look at the best new airsoft guns on the market, need a gun upgrade, or just want to browse our inventory of over 2,000 products we are here to help. Whether you're a seasoned veteran or a beginner our staff is here to help answer any questions you may have.


</details>

## The History of Fox Airsoft

<details>
    <summary>History</summary>
 
The Fox Airsoft shop started off in 2006 as a small airsoft store in Castle Rock, Colorado, founded by several members of the airsoft team Swampfox 12. After just a couple of quick short years, Fox Airsoft has grown to become the Ultimate Airsoft Retailer with over 14 years of experience in the industry. Fox Airsoft only carries the highest quality airsoft guns at our airsoft shop. The store itself is continuing to grow and stock more products as we continue to become the ultimate airsoft retailer. Support your local business by shopping at the Fox Airsoft Store. If you are looking for ICS Airsoft Guns we have a sister site that specializes in only those products.  We are also proud to be selling LCT airsoft guns a favorite for the AK lovers.


</details>